
(cl:in-package :asdf)

(defsystem "my_package-msg"
  :depends-on (:roslisp-msg-protocol :roslisp-utils )
  :components ((:file "_package")
    (:file "New_Goal" :depends-on ("_package_New_Goal"))
    (:file "_package_New_Goal" :depends-on ("_package"))
  ))