# generated from catkin/cmake/template/pkg.context.pc.in
CATKIN_PACKAGE_PREFIX = ""
PROJECT_PKG_CONFIG_INCLUDE_DIRS = "/home/oren/pad/devel/include;/home/oren/pad/src/my_package/include".split(';') if "/home/oren/pad/devel/include;/home/oren/pad/src/my_package/include" != "" else []
PROJECT_CATKIN_DEPENDS = "roscpp;rospy;std_msgs;message_runtime".replace(';', ' ')
PKG_CONFIG_LIBRARIES_WITH_PREFIX = "-lmy_package".split(';') if "-lmy_package" != "" else []
PROJECT_NAME = "my_package"
PROJECT_SPACE_DIR = "/home/oren/pad/devel"
PROJECT_VERSION = "0.0.0"
