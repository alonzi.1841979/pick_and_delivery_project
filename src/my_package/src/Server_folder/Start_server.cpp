#include <iostream>
#include <sys/types.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <fstream>
#include <sstream>
#include <stdio.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <cstring>
#include <string>
#include <pthread.h>
#include <netinet/in.h>
#include <signal.h>
#include <cppconn/driver.h>
#include <cppconn/connection.h>
#include <cppconn/exception.h>
#include <cppconn/statement.h>
#include <cppconn/resultset.h>
#include <cppconn/resultset_metadata.h>
#include <mysql_connection.h>
#include <cstddef>

  pthread_t thread_tcp [100], thread_file [10];
  int threadno_tcp = 0;

  int listening;
  
  std::string robot_occupied = "N";

  struct req {
     int des;
     socklen_t addlen;
     sockaddr_in clientaddr;
  };

  void* tcp_connection (void*);

    sql::Driver* driver;
    sql::Connection* connection;
    sql::Statement* stmt;
    sql::ResultSet* result;
    sql::ResultSetMetaData* res_md;
    std::string query = "SELECT * FROM stanze WHERE occupata = 1";

    const std::string user = "root";
    const std::string database = "pad";
    const std::string password = "pad";
    const std::string dbhost = "tcp://127.0.0.1:9003";

using namespace std;

void sig_handler(int signo) {

     /* signal handler */
     if (signo == SIGINT) {
         cout << "\t Exiting..." << '\n';
         close(listening);
         exit(1);
     }
 }

int main(int argc, char const *argv[]){

  /*it creates the shared memory*/
  int res;
  unlink("dest_fifo");
  res = mkfifo("dest_fifo",0666);
  if (res==-1) {
    cerr << "Errore creazione fifo" << endl;
    return EXIT_FAILURE;
  }
  unlink("ack_fifo");
  res = mkfifo("ack_fifo",0666);
  if (res==-1) {
    cerr << "Errore creazione fifo" << endl;
    return EXIT_FAILURE;
  }
  

  /*starts server*/
  /*enables tcp socket connections with clients*/
  /*writes on dest fifo info sent by clients*/
  /*handles cuncurrent(invalid) client's requests*/
  
  // Create a socket
    listening = socket(AF_INET, SOCK_STREAM, 0);
    if (listening == -1)
    {
        cerr << "Can't create a socket! Quitting" << endl;
        return -1;
    }
 
    // Bind the ip address and port to a socket
    sockaddr_in hint;
    hint.sin_family = AF_INET;
    hint.sin_port = htons(9002);
    inet_pton(AF_INET, "127.0.0.1", &hint.sin_addr); //localhost
 
    bind(listening, (sockaddr*)&hint, sizeof(hint));
 
    // Tell Winsock the socket is for listening
    listen(listening, SOMAXCONN);

    /* Signal catching */
    signal(SIGINT, sig_handler);
    signal(SIGTSTP, sig_handler);
 
    // Wait for a connection
    sockaddr_in client;
    socklen_t clientSize = sizeof(client);
	int clientSocket;
 
    while (true)
    {

	    clientSocket = accept(listening, (sockaddr*)&client, &clientSize);
	
 
    	char host[NI_MAXHOST];      // Client's remote name
    	char service[NI_MAXSERV];   // Service (i.e. port) the client is connect on
 
 	    memset(host, 0, NI_MAXHOST);
    	memset(service, 0, NI_MAXSERV);
 
    	if (getnameinfo((sockaddr*)&client, sizeof(client), host, NI_MAXHOST, service, NI_MAXSERV, 0) == 0)
    	{
        	cout << host << " connected on port " << service << endl;
    	}
    	else
    	{
        	inet_ntop(AF_INET, &client.sin_addr, host, NI_MAXHOST);
        	cout << host << " connected on port " << ntohs(client.sin_port) << endl;
    	}
	
	    req *arg = new req;  // allocate memory
      	bzero (arg, sizeof (req));  // Clear memory
      	arg->addlen = clientSize;
      	arg->clientaddr = client;
      	arg->des = clientSocket;

	pthread_create (&thread_tcp [threadno_tcp++], NULL, tcp_connection, (void*)arg);
      	if (threadno_tcp == 100)
          threadno_tcp = 0;
    }
}

void* tcp_connection (void*  arg) {
     req sock = *((req*)arg);
     char buf[300]; 
     memset(buf, 0, 300);
     
     try
	    {
	        // stabiliamo la connesione e selez. il db
	        driver = get_driver_instance();
	        connection = driver->connect(dbhost, user, password);
	        connection->setSchema(database);
	        // creiamo uno statement
	        stmt = connection->createStatement();
	        // eseguiamo la query
	        result = stmt->executeQuery(query);
	        // otteniamo i metadati
	        res_md = result->getMetaData();
	        // contiamo le righe ottenute
	        cout << ">> Presi "
	            << result->rowsCount()
	            << " record <<"
	            << endl << endl;
	        // fetch dei dati
	        char stanze_occ[300]; 
			memset(stanze_occ, 0, 300);
			int i = 0;
	        while(result->next())
	        {
	               std::string occupate = result->getString(1);
	               cout << occupate << endl;
	               stanze_occ[i] = occupate[occupate.length() -1 ];
	               i++;
	        }
	        send(sock.des, stanze_occ, i + 1, 0);
	        // liberiamo le risorse
	    }
	catch(sql::SQLException& e)
	    {
                cout << e.what() << endl;	    
        }
	
		std::string stanza; char* ptr; char coord[300]; 
     while (1) {
	    // Wait for client to send data
        int bytesReceived = recv(sock.des, buf, 300,0);                    
        if (bytesReceived == -1)
        {
				fprintf(stderr, "socket() failed: %s\n", strerror(errno));
				if (stanza!="")stmt->execute("UPDATE `stanze` SET `occupata`='0' WHERE `nome` = '"+stanza+"'");
				cerr << "Error in recv(). Quitting" << endl;
				break;
        }
        cerr << "PASSED" << endl;
		std::string rec = string(buf, 0, bytesReceived);
		cout << rec << endl;
		char p[rec.length() +1];
		strcpy(p, rec.c_str());
		std::string sel = "Selection";
		std::string ex = "Exiting";
		std::string get = "Get";
		std::string call = "Call";
		std::string set = "Set";
		std::string Send = "Send";
		std::string acc = "Access";
		char* s =strtok(p, ":");
		cout <<  s << endl;
        if (s == sel)
        {
			ptr = strtok(NULL, "\0");
			stanza = ptr;
			cout << stanza << endl;
			//check db
			result = stmt->executeQuery("SELECT * FROM `stanze` WHERE `occupata` = 1 AND `nome`= '"+stanza+"'");
			if (result->rowsCount() == 0) {
				stmt->execute("UPDATE `stanze` SET `occupata`='1' WHERE `nome` = '"+stanza+"'");
				send(sock.des, "OK", 3, 0);
			}
			else send(sock.des, "NACK", 5, 0);
        }
        else if (s == get || s == set)
        {
			ptr = strtok(NULL, "\0");
			stanza = ptr;
			cout << stanza << endl;
			result = stmt->executeQuery("SELECT * FROM `stanze` WHERE `nome`= '"+stanza+"'");
			std::string co;
			memset(coord, 0, 300);
			while(result->next())
	        {
				cout << "ALL OK" << endl;
				for(int i = 2; i<5; i++) {
	               std::string x = result->getString(i);
	               if (i!=4) x = x+":";
	               cout << x << endl;
				   co+=x;
			   }
	        }
			strcpy(coord, co.c_str());
            cout << coord << endl;
            send(sock.des, coord, co.length()+1, 0);
        }
        else if (s == ex)
        {
			ptr = strtok(NULL, "\0");
			stanza = ptr;
			cout << stanza << endl;
			stmt->execute("UPDATE `stanze` SET `occupata`='0' WHERE `nome` = '"+stanza+"'");
	        break;
            
        }
        else if (s == acc)
        {
			send(sock.des, robot_occupied.c_str(), robot_occupied.length()+1, 0);    
        }
        else if (s == call)
        {  
			robot_occupied = "y"; 
			float* coordinate = (float*)malloc(sizeof(float)*3); int i;
			for (i=0;i<3;i++) {
				if (i==2) ptr = strtok(NULL, "\0");
				else ptr = strtok(NULL, ":");
				std::string coordinata = ptr;
				cout << coordinata << endl;
				float f = std::atof(coordinata.c_str());
				coordinate[i] = f;
			}
				int fd = open("dest_fifo",O_WRONLY);
				if (!fd) {
					cerr << "Errore apertura in scrittura della fifo" << endl;
				    break;
				}
				else {
					for (i=0;i<3;i++) {
					if(write(fd,&coordinate[i],sizeof(float))==-1) {
						cerr << "Errore di scrittura nella fifo" << endl;
						break;
					    }
				    }
				}
				close(fd);
				cout << "ROBOT HAS BEEN CALLED" << endl;
				//check if arrives
				int* consegna = (int*)malloc(sizeof(int));
				int fd2 = open("ack_fifo",O_RDONLY);
				if (!fd2) {
					cerr << "Errore apertura in lettura della fifo" << endl;
					break;
				}
				if(read(fd2,consegna, sizeof(int))==-1) {
					cerr << "Errore di lettura nella fifo" << endl;
					break;
				}
				close(fd2);
				cout << "ROBOT AVAILABLE" << endl;
				std::string av = "Available";
				send(sock.des, av.c_str(), av.length()+1, 0);
				cout << "DATA SENT" << endl;
		}
		else if (s == Send)
        {   
			float* coordinate = (float*)malloc(sizeof(float)*3); int i;
			for (i=0;i<3;i++) {
				if (i==2) ptr = strtok(NULL, "\0");
				else ptr = strtok(NULL, ":");
				std::string coordinata = ptr;
				cout << coordinata << endl;
				float f = std::atof(coordinata.c_str());
				coordinate[i] = f;
			}
				int fd = open("dest_fifo",O_WRONLY);
				if (!fd) {
					cerr << "Errore apertura in scrittura della fifo" << endl;
				    break;
				}
				else {
					for (i=0;i<3;i++) {
					if(write(fd,&coordinate[i],sizeof(float))==-1) {
						cerr << "Errore di scrittura nella fifo" << endl;
						break;
					    }
				    }
				}
				close(fd);
				cout << "ROBOT HAS BEEN SENT" << endl;
				//check if arrives
				int* consegna = (int*)malloc(sizeof(int));
				int fd2 = open("ack_fifo",O_RDONLY);
				if (!fd2) {
					cerr << "Errore apertura in lettura della fifo" << endl;
					break;
				}
				if(read(fd2,consegna, sizeof(int))==-1) {
					cerr << "Errore di lettura nella fifo" << endl;
					break;
				}
				close(fd2);
				cout << "ROBOT ARRIVED" << endl;
				std::string arr = "Arrived";
				send(sock.des, arr.c_str(), arr.length()+1, 0);
				cout << "DATA SENT" << endl;
				fd = open("dest_fifo",O_WRONLY);
				if (!fd) { //reset
					cerr << "Errore apertura in scrittura della fifo" << endl;
				    break;
				}
				else {
					for (i=0;i<3;i++) {
					if(write(fd,&(coordinate[i]+=0.02),sizeof(float))==-1) {
						cerr << "Errore di scrittura nella fifo" << endl;
						break;
					    }
				    }
				}
				close(fd);
				fd2 = open("ack_fifo",O_RDONLY);
				if (!fd2) {
					cerr << "Errore apertura in lettura della fifo" << endl;
					break;
				}
				if(read(fd2,consegna, sizeof(int))==-1) {
					cerr << "Errore di lettura nella fifo" << endl;
					break;
				}
				close(fd2); //reset end
				robot_occupied = "N";
		}
        memset(buf, 0, 300);     
     }
     delete result;
     delete stmt;
	 connection->close();
	 delete connection;
	 close(sock.des);
 }
