import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.*;

class GUI extends JFrame {

    JButton callbtn  = new JButton("Call Robot to your Room");
    JButton sendbtn = new JButton("Send Robot");
    JButton submit = new JButton("Submit Room");

    JRadioButton A = new JRadioButton("Room A");
    JRadioButton B = new JRadioButton("Room B");
    JRadioButton C = new JRadioButton("Room C");
    JRadioButton D = new JRadioButton("Room D");
    JRadioButton E = new JRadioButton("Room E");
    JRadioButton F = new JRadioButton("Room F");
    ButtonGroup group = new ButtonGroup();


    public GUI() {
        
        this.setTitle("Pick and Delivery");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setSize(1000,1000);
        this.setLocation(new Point(250,250));
        this.setLayout(null);
        callbtn.setBounds(50,130, 220,25); //left, right, width and height boundary pixels
        sendbtn.setBounds(50,100, 220,25);
	submit.setBounds(50,150, 220,25); 
	A.setBounds(50,200, 220,25);
	B.setBounds(50,250, 220,25);
	C.setBounds(50,300, 220,25);
	D.setBounds(50,350, 220,25);
	E.setBounds(50, 400, 220,25);
	F.setBounds(50, 450, 220,25);
	submit.addActionListener(new ActionListener(){public void actionPerformed(ActionEvent e) {
	    submit.setEnabled(false); 
    	String selection = group.getSelection().getActionCommand();
	    A.setEnabled(false);
	    B.setEnabled(false);
	    C.setEnabled(false);
	    D.setEnabled(false);
	    E.setEnabled(false);
	    F.setEnabled(false);
	    Client_app.setSelection(selection);
	    callbtn.setEnabled(true);
  	    }
	});
	callbtn.addActionListener(new ActionListener(){public void actionPerformed(ActionEvent e) {
		Client_app.getAccess();
		System.out.println("Getting Access...");
		if (Client_app.can_access==1) {
				callbtn.setEnabled(false);
				Client_app.call_robot();
			}
		else {
			System.out.println("Access Denied");
			JOptionPane.showMessageDialog(null, "Someone else is using the Robot! \nPlease wait...");
			}
	}
	});
	sendbtn.addActionListener(new ActionListener(){public void actionPerformed(ActionEvent e) {
		sendbtn.setEnabled(false);
		String dest = group.getSelection().getActionCommand();
	    A.setEnabled(false);
	    B.setEnabled(false);
	    C.setEnabled(false);
	    D.setEnabled(false);
	    E.setEnabled(false);
	    F.setEnabled(false);
	    Client_app.setDest(dest);
	    JOptionPane.showMessageDialog(null, "Robot has been sent! \nYou can call it again when it has done delivering...");
	    }
	});
	this.add(submit);
        this.add(callbtn);
        this.add(sendbtn);
	this.add(A);
	this.add(B);
	this.add(C);
	this.add(D);
	this.add(E);
	this.add(F);
	callbtn.setEnabled(false);
	sendbtn.setEnabled(false);
        this.A.setActionCommand("Room A");
	this.B.setActionCommand("Room B");
	this.C.setActionCommand("Room C");
        this.D.setActionCommand("Room D");
	this.E.setActionCommand("Room E");
	this.F.setActionCommand("Room F");
    	group.add(A);
    	group.add(B);
    	group.add(C);
    	group.add(D);
    	group.add(E);
    	group.add(F);
    }
}
