import java.io.*;
import java.net.*;
import javax.swing.*;
import java.lang.*;

public class Client_app {

	static String room;
	static String dest_room;

	static float[] room_coord = new float[3];
	static float[] dest_room_coord = new float[3];
	
	static PrintStream PS = null;
	static InputStreamReader in = null;
	
	static int can_access = 0;
	
	public static void ext(Socket s) {
		Runtime.getRuntime().addShutdownHook(new Thread() {
    	public void run() {
		if (room!=null) PS.println("Exiting:"+room);
		else PS.println("Exiting:0");
		try {
			Thread.sleep(5);
			s.close();
 		} catch (IOException ex) {
            System.out.println(ex);
       		} catch(InterruptedException e) {
			System.out.println(e);
			}
		}
 	    });
	}

	public static void setSelection(String sel) {
		room=sel;
	}
	
	public static void safeExit() {
		PS.println("Exiting:"+room);
	}
	
	public static void setS(PrintStream P, InputStreamReader i) {
		PS=P;
		in=i;
	}
	
	public static void getAccess() {
		PS.println("Access:");
		try {
			Thread.sleep(5);
			} catch(InterruptedException ex) {
			System.out.println(ex);
		}
	}
	
	public static void call_robot() {
		PS.println("Call:"+Float.toString(room_coord[0])+":"+Float.toString(room_coord[1])+":"+Float.toString(room_coord[2]));
	}
	
	public static void setDest(String dest) {
		dest_room = dest;
		PS.println("Set:"+dest_room);
		try {
			Thread.sleep(5);
			} catch(InterruptedException ex) {
			System.out.println(ex);
		}
	}
	
	public static void send_robot() {
		PS.println("Send:"+Float.toString(dest_room_coord[0])+":"+Float.toString(dest_room_coord[1])+":"+Float.toString(dest_room_coord[2]));
	}

    public static void main(String[] args) { 

        GUI Java_Gui = new GUI();
        Java_Gui.setVisible(true);
	 

        String ip = "127.0.0.1";
        int port = 9002;
        try {            
            java.net.Socket socket = new java.net.Socket(ip,port);
            PrintStream PS =new PrintStream(socket.getOutputStream());
            InputStreamReader in = new InputStreamReader(socket.getInputStream());
            
        setS(PS, in);
	    ext(socket);

            //send
            PS.println("HELLO TO SERVER FROM CLIENT");

            BufferedReader br = new BufferedReader(in);
            char[] buffer = new char[300];
            int count = br.read(buffer, 0, 300);
            String reply = new String(buffer, 0, count);
            System.out.println(reply);
	    char c;
		for (int i=0; i < reply.length(); i++) {
   			c = reply.charAt(i);
			if (c=='A') Java_Gui.A.setEnabled(false);
			else if (c=='B') Java_Gui.B.setEnabled(false);
			else if (c=='C') Java_Gui.C.setEnabled(false);
			else if (c=='D') Java_Gui.D.setEnabled(false);
			else if (c=='E') Java_Gui.E.setEnabled(false);
			else if (c=='F') Java_Gui.F.setEnabled(false);
		}
	    System.out.println("Room checked");
	    //waiting for the user to select the room
	    while(true) {
		try {
			Thread.sleep(5);
		} catch(InterruptedException ex) {
			System.out.println(ex);
		}
		if (room != null) {
			PS.println("Selection:"+room);
			System.out.println(room+" sent to server");
			try {
				Thread.sleep(5);
			} catch(InterruptedException ex) {
				System.out.println(ex);
			}
			count = br.read(buffer, 0, 300);
            reply = new String(buffer, 0, count);
			System.out.println(reply);
			if (count==3) {
				PS.println("Get:"+room);
				try {
					Thread.sleep(5);
				} catch(InterruptedException ex) {
					System.out.println(ex);
				}
				count = br.read(buffer, 0, 300);
            	reply = new String(buffer, 0, count);
				System.out.println(reply);
				String[] parts = reply.split(":");
				String part1 = parts[0]; //1
				try {
					String part2 = parts[1]; //2
					String part3 = parts[2]; //3
					room_coord[0] = Float.parseFloat(part1);
					room_coord[1] = Float.parseFloat(part2);
					room_coord[2] = Float.parseFloat(part3);
					System.out.println(part1+" "+part2+" "+part3);
				} catch(java.lang.ArrayIndexOutOfBoundsException exept) {
					System.out.println(exept);
					safeExit();
					System.exit(1);
				}
				break;
			}
			else if (count==5){
				System.out.println("Room Already Taken");
				room = null;
				Java_Gui.A.setEnabled(true);
				Java_Gui.B.setEnabled(true);
				Java_Gui.C.setEnabled(true);
				Java_Gui.D.setEnabled(true);
				Java_Gui.E.setEnabled(true);
				Java_Gui.F.setEnabled(true);
				Java_Gui.submit.setEnabled(true);
				Java_Gui.callbtn.setEnabled(false);
			}
		}	
	   }
            BufferedReader buffr = new BufferedReader(in);
			char[] buff = new char[300];
            while(true) {
				try {
				int byter = buffr.read(buff, 0, 300);
               String resp = new String(buff, 0, byter); //server has sent reply after reading ack fifo
               System.out.println(resp);
               String av = "Available";
               String ar = "Arrived";
			   if (resp.length() == 10) {
				   System.out.println(resp);
				   Java_Gui.sendbtn.setEnabled(true);
				   if (Java_Gui.A.getText().equals(room))Java_Gui.A.setEnabled(false);
				   else Java_Gui.A.setEnabled(true);
				   if (Java_Gui.B.getText().equals(room))Java_Gui.B.setEnabled(false);
				   else Java_Gui.B.setEnabled(true);
				   if (Java_Gui.C.getText().equals(room))Java_Gui.C.setEnabled(false);
				   else Java_Gui.C.setEnabled(true);
				   if (Java_Gui.D.getText().equals(room))Java_Gui.D.setEnabled(false);
				   else Java_Gui.D.setEnabled(true);
				   if (Java_Gui.E.getText().equals(room))Java_Gui.E.setEnabled(false);
				   else Java_Gui.E.setEnabled(true);
				   if (Java_Gui.F.getText().equals(room))Java_Gui.F.setEnabled(false);
				   else Java_Gui.F.setEnabled(true);
				   
			   }
			   else if (resp.length() == 8) {
				   Java_Gui.callbtn.setEnabled(true);
			   }
			   else if (resp.length() == 2) {
				   System.out.println(buff[0]);
				   if (buff[0]=='N') can_access = 1;
				   else can_access = 0;
			   }
			   else if (resp.length() > 10) {
					String[] parti = resp.split(":");
					String parte1 = parti[0]; //1
					try {
						String parte2 = parti[1]; //2
						String parte3 = parti[2]; //3
						dest_room_coord[0] = Float.parseFloat(parte1);
						dest_room_coord[1] = Float.parseFloat(parte2);
						dest_room_coord[2] = Float.parseFloat(parte3);
						System.out.println(parte1+" "+parte2+" "+parte3);
						send_robot();
					} catch(java.lang.ArrayIndexOutOfBoundsException exept) {
						System.out.println(exept);
						safeExit();
						System.exit(1);
					}
				  }
			   } catch (IOException exept) {
					System.out.println(exept);
					safeExit();
					System.exit(1);
				}
            }
        } catch (IOException ex) {
            System.out.println(ex);
            safeExit();
			System.exit(1);
        }

    }
}      
