#include "ros/ros.h"
#include <nav_msgs/Odometry.h>
#include <sensor_msgs/LaserScan.h>
#include <tf2_msgs/TFMessage.h>
#include <geometry_msgs/TransformStamped.h>
#include <tf2_ros/transform_listener.h>
#include <tf/tf.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <geometry_msgs/PoseStamped.h>
#include <my_package/New_Goal.h>

  using namespace std;
  using namespace ros;

  std::vector<float> target_position(2,0);
  std::vector<float> old_position(2,0);
  std::vector<float> current_position(2,0);

  geometry_msgs::PoseStamped new_goal_msg;
  tf2_ros::Buffer tfBuffer;

  size_t n = 10;
  int message_published = 0;
  int moving = 0;
  int delivering = 0; /*serve ad avvisare il client dell'avvenuta consegna*/

  int T = 2;

  int fd;
   
  void Set_New_Goal_Callback(const my_package::New_Goal& msg)
  { 
    if (moving==0) {
        new_goal_msg.header.seq = n;
        n++;
    
        new_goal_msg.header.stamp = ros::Time::now();
        new_goal_msg.header.frame_id = "map";
    
        new_goal_msg.pose.position.x = msg.x;
        new_goal_msg.pose.position.y = msg.y;
        new_goal_msg.pose.position.z = 0;

        new_goal_msg.pose.orientation.x = 0;
        new_goal_msg.pose.orientation.y = 0;
        new_goal_msg.pose.orientation.z = 0;
        new_goal_msg.pose.orientation.w = msg.theta;
    
        message_published = 1; /*pubblica il new goal una volta sola per ogni richiesta*/
        moving = 1;

        target_position[0] = new_goal_msg.pose.position.x;
        target_position[1] = new_goal_msg.pose.position.y;
        ROS_INFO("SETTO IL GOAL");
    }
  }

  void position_Callback(const tf2_msgs::TFMessage& tf) {
    
    int got_transform;
    got_transform = tfBuffer.canTransform("map", "base_link", ros::Time(0));
    if (got_transform != 0) {
        geometry_msgs::TransformStamped transformStamped;
        transformStamped = tfBuffer.lookupTransform("map", "base_link", ros::Time(0));   /*ottengo la trasformata della posizione attuale*/
        
        current_position[0] = transformStamped.transform.translation.x;
		current_position[1] = transformStamped.transform.translation.y;
    }

}

  void check1_CallBack(const ros::TimerEvent event) {
    
    if(moving!=0) {
        ROS_INFO("Check if I'm moving");
        cerr << current_position[0] << endl;
        cerr << current_position[1] << endl;
        cerr << target_position[0] << endl;
        cerr << target_position[1] << endl;
        float distance;
        distance = sqrt(pow(current_position[0]-old_position[0],2)+pow(current_position[1]-   old_position[1],2));
        if (distance < 0.5) {
            ROS_INFO("Blocked");
        }
        if (sqrt(pow(current_position[0]-target_position[0],2)+pow(current_position[1]-   target_position[1],2)) < 1.5) {
            if(delivering==1) {
                ROS_INFO("Robot has Delivered");
                /* avviso il client che il pacco e' stato ricevuto scrivendolo nella fifo*/
                int fdd=open("/home/oren/pad/src/my_package/src/Server_folder/ack_fifo", O_WRONLY);
                int del = 2;
                if (!fdd) {
                    cerr << "Errore apertura in scrittura della fifo" << endl;
                }
                if(write(fdd, &del, sizeof(int)==-1)) {
                    cerr << "Errore di scrittura nella fifo" << endl;
                }
                close(fdd);
                delivering = 0;
                moving=0;
            }
            else {
                ROS_INFO("Robot has arrived at client's room");
                int fda=open("/home/oren/pad/src/my_package/src/Server_folder/ack_fifo", O_WRONLY);
                int arr = 1;
                if (!fda) {
                    cerr << "Errore apertura in scrittura della fifo" << endl;
                }
                if(write(fda, &arr, sizeof(int)==-1)) {
                    cerr << "Errore di scrittura nella fifo" << endl;
                }
                close(fda);
                delivering=1;
                moving=0;
            }            
        }
    }
    
  }

  void check2_CallBack(const ros::TimerEvent event) {
    
    if (moving!=0) {
        float distance;
        distance = sqrt(pow(current_position[0]-target_position[0],2)+pow(current_position[1]-target_position[1],2));
        if (distance > 0.5) {
            ROS_INFO("TIMEOUT: Could not reach Goal");
        }
    }
    
  }
   
  int main(int argc, char **argv)
  {
 
    ros::init(argc, argv, "Set_New_Goal");
   
    ros::NodeHandle n;
    ros::Publisher pub = n.advertise<geometry_msgs::PoseStamped>("/move_base_simple/goal", 1000);
    
    tf2_ros::TransformListener tfListener(tfBuffer);

    ros::Rate loop_rate(T);
   
    ros::Subscriber sub = n.subscribe("New_Goal", 1000, Set_New_Goal_Callback);
    ros::Subscriber sub_tf = n.subscribe("tf", 1000, position_Callback);

    ros::Timer timer1 = n.createTimer(ros::Duration(1), check1_CallBack);
    ros::Timer timer2 = n.createTimer(ros::Duration(60), check2_CallBack);

    int count=0;
    while(ros::ok()) {
        if(message_published == 1) {
			pub.publish(new_goal_msg);
			ROS_INFO("Publishing New Goal");
            message_published=0;
        }   
  
        ros::spinOnce();
        loop_rate.sleep();
        ++count;
   }
    return 0;
  }
