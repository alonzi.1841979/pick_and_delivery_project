#include <iostream>
#include <fstream>
#include <sstream>
#include <stdio.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>
#include "ros/ros.h"
#include <string.h>
#include <nav_msgs/Odometry.h>
#include <geometry_msgs/TransformStamped.h>
#include <geometry_msgs/PoseStamped.h>
#include <my_package/New_Goal.h>

my_package::New_Goal msg;
int fd;
int i;

using namespace std;
using namespace ros;

int main(int argc, char**argv) {
    
    ros::init(argc,argv,"Send_New_Goal");
    ros::NodeHandle n;
    
    ros::Publisher pub = n.advertise<my_package::New_Goal>("New_Goal",1000);

    fd = open("/home/oren/pad/src/my_package/src/Server_folder/dest_fifo", O_RDONLY);
    if (fd==-1) {
        cerr << "Errore apertura in lettura della fifo" << endl;
        return EXIT_FAILURE;
    }
    
    float dest[3]; float prev_dest[2]; prev_dest[0]=-1.0; prev_dest[1]=-1.0;

    int count=0;
    while(ros::ok()) {

        for (i=0;i<3;i++) {
            if(read(fd,&dest[i], sizeof(float))==-1) {
                cerr << "Errore di lettura nella fifo" << endl;
                return EXIT_FAILURE;
            }
        }

        msg.x = dest[0];
        msg.y = dest[1];
        msg.theta = dest[2];

		if (dest[0]!=prev_dest[0] && dest[1]!=prev_dest[1]) {
			pub.publish(msg);
			prev_dest[0] = dest[0];
			prev_dest[1] = dest[1];
		}
		else cerr << "STESSI VALORI" << endl;	
		    
        ros::spinOnce();
        sleep(5);

        }

    close(fd);

       return 0;
    }
